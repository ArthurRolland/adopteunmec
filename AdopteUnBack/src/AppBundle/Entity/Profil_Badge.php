<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Profil_Badge
 *
 * @ORM\Table(name="profil__badge")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Profil_BadgeRepository")
 */
class Profil_Badge
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="enable", type="boolean")
     */
    private $enable;

    /**
     * @ORM\ManyToOne(targetEntity="Badge")
     */
    private $badge;

    /**
     * @ORM\ManyToOne(targetEntity="Profil", inversedBy="profilBadges")
     */
    private $profil;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set unable
     *
     * @param boolean $enable
     *
     * @return Profil_Badge
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;

        return $this;
    }

    /**
     * Get enable
     *
     * @return bool
     */
    public function getEnable()
    {
        return $this->enable;
    }



    /**
     * Set badge
     *
     * @param \AppBundle\Entity\Badge $badge
     *
     * @return Profil_Badge
     */
    public function setBadge(\AppBundle\Entity\Badge $badge = null)
    {
        $this->badge = $badge;

        return $this;
    }

    /**
     * Get badge
     *
     * @return \AppBundle\Entity\Badge
     */
    public function getBadge()
    {
        return $this->badge;
    }

    /**
     * Set profil
     *
     * @param \AppBundle\Entity\Profil $profil
     *
     * @return Profil_Badge
     */
    public function setProfil(\AppBundle\Entity\Profil $profil = null)
    {
        $this->profil = $profil;

        return $this;
    }

    /**
     * Get profil
     *
     * @return \AppBundle\Entity\Profil
     */
    public function getProfil()
    {
        return $this->profil;
    }
}
