<?php

namespace App\Repository;

use App\Entity\ProfilBadge;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProfilBadge|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProfilBadge|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProfilBadge[]    findAll()
 * @method ProfilBadge[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfilBadgeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProfilBadge::class);
    }

    // /**
    //  * @return ProfilBadge[] Returns an array of ProfilBadge objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProfilBadge
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
